package com.swardana.wisteria.tools;

import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrowsExactly;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

/**
 * Unit test for {@link Assert}.
 *
 * @author Sukma Wardana
 * @version 1.0
 * @since 1.0
 */
class AssertTest {


	@Nested
	class IsTrue {

		@Test
		void doNothingWhenTheConditionIsTrue() {
			assertDoesNotThrow(() -> Assert.isTrue(true));
		}

		@Test
		void exceptionThrownWhenTheConditionIsFalse() {
			assertThrowsExactly(Assert.Failed.class, () -> Assert.isTrue(false));
		}

		@Test
		void exceptionThrownWithMessageWhenTheConditionIsFalse() {
			assertThatThrownBy(() -> Assert.isTrue(false, "Bad condition!"))
				.isInstanceOf(Assert.Failed.class)
				.hasMessage("Bad condition!");
		}
	}

	@Nested
	class IsFalse {

		@Test
		void doNothingWhenTheConditionIsFalse() {
			assertDoesNotThrow(() -> Assert.isFalse(false));
		}

		@Test
		void exceptionThrownWhenTheConditionIsTrue() {
			assertThrowsExactly(Assert.Failed.class, () -> Assert.isFalse(true));
		}

		@Test
		void exceptionThrownWithMessageWhenTheConditionIsTrue() {
			assertThatThrownBy(() -> Assert.isFalse(true, "Bad condition!"))
				.isInstanceOf(Assert.Failed.class)
				.hasMessage("Bad condition!");
		}
	}

}
