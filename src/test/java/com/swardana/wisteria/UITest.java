package com.swardana.wisteria;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Optional;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

/**
 * Unit test for {@link UI}.
 *
 * @author Sukma Wardana
 * @version 1.0
 * @since 1.0
 */
class UITest {

	private static UI mockUI;

	@BeforeAll
	static void prepare() {
		mockUI = mock(UI.class);
	}

	@AfterAll
	static void tearDown() {
		mockUI = null;
	}

	@Nested
	class ReadOnlyView {

		@Test
		void returnOptionalEmptyWhenVisualProxyIsNotExist() {
			when(mockUI.visual(anyString(), anyBoolean())).thenReturn(Optional.empty());
			assertThat(mockUI.visual(anyString())).isEmpty();
		}

	}

	@Nested
	class ParentView {

		@Test
		void throwNoSuchElementExceptionWhenDefaultVisualProxyNotExist() {
			when(mockUI.visual(anyString(), anyBoolean())).thenReturn(Optional.empty());
			assertThat(mockUI.visual()).isNull();
		}

	}

}
