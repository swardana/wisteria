package com.swardana.wisteria;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

/**
 * Unit test for default implementation of {@link ControlTower}.
 *
 * @author Sukma Wardana
 * @version 1.0
 * @since 1.0
 */
class ControlTowerTest {

	private static final String TOPIC = "topic";
	private static final String ANOTHER_TOPIC = "second_" + TOPIC;
	private static final Object[] MESSAGES = new String[]{"hello", "world!"};

	private Subscriber john;
	private Subscriber adam;
	private Subscriber lea;

	@BeforeEach
	void setUp() {
		john = mock(Subscriber.class);
		adam = mock(Subscriber.class);
		lea = mock(Subscriber.class);
	}

	@AfterEach
	void tearDown() {
		john = null;
		adam = null;
		lea = null;
		ControlTower.Factory.get().clear();
	}

	@Nested
	class Publish {

		@Test
		void shouldPublishMessageForSubscriber() {
			ControlTower.Factory.get().subscribe(TOPIC, john);
			ControlTower.Factory.get().publish(TOPIC, MESSAGES);
			verify(john, times(1)).receive(TOPIC, MESSAGES);
		}

		@Test
		void shouldOnlyConsumeMessageFromTheEvent() {
			ControlTower.Factory.get().subscribe(TOPIC, john);
			ControlTower.Factory.get().subscribe(TOPIC, adam);
			ControlTower.Factory.get().subscribe(ANOTHER_TOPIC, lea);
			ControlTower.Factory.get().publish(TOPIC, MESSAGES);
			verify(john, times(1)).receive(TOPIC, MESSAGES);
			verify(adam, times(1)).receive(TOPIC, MESSAGES);
			verify(lea, times(0)).receive(TOPIC, MESSAGES);
			verify(lea, times(0)).receive(ANOTHER_TOPIC, MESSAGES);
		}

		@Test
		void subscribeMoreThanOneEvent() {
			ControlTower.Factory.get().subscribe(TOPIC, adam);
			ControlTower.Factory.get().subscribe(ANOTHER_TOPIC, adam);
			ControlTower.Factory.get().publish(TOPIC, MESSAGES);
			ControlTower.Factory.get().publish(ANOTHER_TOPIC, MESSAGES);
			verify(adam, times(2)).receive(any(), any());
		}

	}

	@Nested
	class Observer {

		@Test
		void shouldOnlySendMessageToSubscriber() {
			ControlTower.Factory.get().subscribe(TOPIC, john);
			ControlTower.Factory.get().subscribe(TOPIC, adam);
			ControlTower.Factory.get().subscribe(TOPIC, lea);
			ControlTower.Factory.get().unsubscribe(TOPIC, adam);
			ControlTower.Factory.get().unsubscribe(TOPIC, lea);
			ControlTower.Factory.get().publish(TOPIC, MESSAGES);
			verify(john, times(1)).receive(TOPIC, MESSAGES);
			verify(adam, times(0)).receive(TOPIC, MESSAGES);
			verify(lea, times(0)).receive(TOPIC, MESSAGES);
		}

	}

}
