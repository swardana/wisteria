package com.swardana.wisteria;

import java.util.NoSuchElementException;
import java.util.Optional;
import javafx.scene.Node;
import javafx.scene.Parent;

/**
 * A user interface visual proxy.
 * <p>
 *     Objects that implement a {@code UI} return proxies (<i>views</i>)
 *     for attributes of a <i>business</i> object when asked.
 * </p>
 *
 * @author Sukma Wardana
 * @version 1.0
 * @since 1.0
 */
public interface UI {

	/**
	 * Return a visual proxy.
	 * <p>
	 *     Return the visual proxy for the <i>business</i> attribute
	 *     specified as an argument or {@code null} if the requested
	 *     attribute isn't recognized.
	 * </p>
	 * <p>
	 *     <b>MUST</b> support {@code null} <i>attribute</i> argument
	 *     which will return the <i>default</i> view, or whatever
	 *     that is.
	 * </p>
	 *
	 * @param attribute the object <i>business</i> attribute represents
	 *                  this proxy or {@code null} fo the default type.
	 * @param isReadOnly if {@code true} then the proxy is non-editable.
	 * @return the proxy, or {@code null} if the requested attribute
	 * is not supported or recognized.
	 */
	Optional<Node> visual(String attribute, boolean isReadOnly);

	/**
	 * Return a non-editable visual proxy.
	 *
	 * @param attribute the object <i>business</i> attribute represents
	 *                  this proxy or {@code null} fo the default type.
	 * @return the non-editable visual proxy, or {@code null} if the
	 * requested attribute is not supported or recognized.
	 */
	default Optional<Node> visual(String attribute) {
		return this.visual(attribute, true);
	}

	/**
	 * Return the default visual proxy.
	 * <p>
	 *     Most the time, the default <i>view</i> is a {@link Parent}.
	 * </p>
	 *
	 * @return the visual proxy.
	 * @throws NoSuchElementException if couldn't return
	 * the {@link Parent}.
	 */
	default Parent visual() {
		return (Parent) this.visual(null, true).orElseThrow();
	}

}
