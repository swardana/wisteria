/**
 * The set of generic tools.
 * <p>
 *     Little tools of general utility that should be of use more than
 *     the current application.
 * </p>
 *
 * @author Sukma Wardana
 * @since 1.0
 */
package com.swardana.wisteria.tools;
