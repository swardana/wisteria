package com.swardana.wisteria.tools;

/**
 * An implementation of an <b>assertion</b>.
 * <p>
 *     Lets you implements preconditions and post conditions.
 * </p>
 *
 * @author Sukma Wardana
 * @version 1.0
 * @since 1.0
 */
public final class Assert {

	private Assert() {}

	public static void isTrue(final boolean expression) {
		if (!expression) {
			throw new Failed();
		}
	}

	public static void isTrue(final boolean expression, final String message) {
		if (!expression) {
			throw new Failed(message);
		}
	}

	public static void isFalse(final boolean expression) {
		if (expression) {
			throw new Failed();
		}
	}

	public static void isFalse(final boolean expression, final String message) {
		if (expression) {
			throw new Failed(message);
		}
	}

	public static class Failed extends RuntimeException {

		Failed() {
			super("Assert failed!");
		}

		Failed(final String msg) {
			super(msg);
		}

	}

}
