package com.swardana.wisteria;

import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.VBox;

/**
 * A menu site widget.
 * <p>
 *     This widget work as container of {@link MenuSite}.
 *     The root pane could directly add this and positioning
 *     as needed on the root pane.
 * </p>
 * <p>
 *     This widget also {@code implements} {@link MenuSite},
 *     so it could be passed as arguments parameter on whichever
 *     object that want to negotiate.
 * </p>
 * <pre>{@code
 * public class MainPane extends BorderPane {
 *
 *     private final MenuWidget menu;
 *
 *     public MainPane() {
 *         this.menu = new MenuWidget();
 *         initGraphics();
 *     }
 *
 *     private void initGraphics() {
 *         this.setTop(this.menu);
 *     }
 * }
 * }</pre>
 *
 * @author Sukma Wardana
 * @version 1.0
 * @since 1.0
 */
public class MenuWidget extends VBox implements MenuSite {

	private final MenuSite support;

	/**
	 * Creates new MenuWidget.
	 */
	public MenuWidget() {
		this.support = new MenuSite.Implementation(this);
	}

	@Override
	public final void addMenu(final Object requester, final Menu menu) {
		this.support.addMenu(requester, menu);
	}

	@Override
	public final void addLineItem(
		final Object requester,
		final MenuItem item,
		final String toThisMenu
	) {
		this.support.addLineItem(requester, item, toThisMenu);
	}

	@Override
	public final void removeMyMenus(final Object requester) {
		this.support.removeMyMenus(requester);
	}
}
