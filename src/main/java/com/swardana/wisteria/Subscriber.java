package com.swardana.wisteria;

/**
 * A subscriber that interested with {@link ControlTower} topic.
 *
 * @author Sukma Wardana
 * @version 1.0
 * @since 1.0
 */
public interface Subscriber {

	/**
	 * Receive the messages which are posted by the {@link ControlTower}.
	 *
	 * @param topic the topic name.
	 * @param messages the payload messages.
	 */
	void receive(String topic, Object[] messages);

}
