package com.swardana.wisteria;

import static java.lang.System.Logger.Level.WARNING;

import java.lang.System.Logger;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * A central tower to managing the communication.
 * <p>
 *     The {@code ControlTower} is a combination of <b>Mediator</b>
 *     and <b>Observer</b> pattern. The {@code ControlTower}
 *     is a mediator for {@code Subscriber} that could subscribe
 *     and unsubscribe on specific topic.
 * </p>
 * <p>
 *     The {@link ControlTower} provide default implementation for basis use.
 *     Get the <i>Singleton</i> instance of {@link ControlTower} with this:
 * </p>
 * <pre>{@code
 * ControlTower ct = ControlTower.Factory.get();
 * ct.subscribe("test", new Subscriber());
 * ct.publish("test", "hello", "world!");
 * }</pre>
 *
 * @author Sukma Wardana
 * @version 1.0
 * @since 1.0
 */
public interface ControlTower {

	/**
	 * Subscribe the {@link Subscriber} to the {@code topic}.
	 *
	 * @param topic the topic name.
	 * @param subscriber the subscriber that interests with the topic.
	 */
	void subscribe(String topic, Subscriber subscriber);

	/**
	 * Unsubscribe the {@link Subscriber} from the {@code topic}.
	 *
	 * @param topic the topic name.
	 * @param subscriber the subscriber who wants to unsubscribe
	 *                   from the topic.
	 */
	void unsubscribe(String topic, Subscriber subscriber);

	/**
	 * Publish messages to the {@code topic}.
	 *
	 * @param topic the topic name.
	 * @param messages the payload messages.
	 */
	void publish(String topic, Object[] messages);

	/**
	 * Clears all {@link Subscriber} subscriptions.
	 * <p>
	 *     Calling {@code clear()} will remove all subscriptions
	 *     in this current instance for session aware applications
	 *     that needs to perform a complete reset.
	 *     e.g.: during logout.
	 * </p>
	 */
	void clear();

	/**
	 * Provide an instances for {@link ControlTower}.
	 *
	 * @author Sukma Wardana
	 * @version 1.0
	 * @since 1.0
	 */
	final class Factory {
		private static ControlTower control;
		private Factory() {}
		public static synchronized ControlTower get() {
			if (control == null) {
				control = new Implementation();
			}
			return control;
		}
	}

	/**
	 * An internal implementation of {@link ControlTower}.
	 * <p>
	 *     This class used as internal machination for {@link ControlTower}
	 *     build with basic functionality wise.
	 * </p>
	 *
	 * @author Sukma Wardana
	 * @version 1.0
	 * @since 1.0
	 */
	final class Implementation implements ControlTower {

		private static final Logger LOG = System.getLogger(Implementation.class.getName());

		private final Map<String, List<Subscriber>> observers = new HashMap<>();

		@Override
		public void subscribe(final String topic, final Subscriber subscriber) {
			if (!this.observers.containsKey(topic)) {
				/*
				 * Use {@link CopyOnWriteArrayList} to prevent
				 * {@link ConcurrentModificationException} if inside
				 * a listeners a new listener is subscribed.
				 */
				this.observers.put(topic, new CopyOnWriteArrayList<>());
			}
			final List<Subscriber> subscribers = this.observers.get(topic);
			if (subscribers.contains(subscriber)) {
				LOG.log(
					WARNING,
					"Subscribe the subscriber [{0}] for the topic [{1}], "
						+ "but the same subscriber was already added "
						+ "for this topic in the past.",
					new Object[]{subscriber, topic}
				);
			}
			subscribers.add(subscriber);
		}

		@Override
		public void unsubscribe(final String topic, final Subscriber subscriber) {
			if (this.observers.containsKey(topic)) {
				final List<Subscriber> subscribers = this.observers.get(topic);
				subscribers.removeIf(pre -> pre.equals(subscriber));
				if (subscribers.isEmpty()) {
					this.observers.remove(topic);
				}
			}
		}

		@Override
		public void publish(final String topic, final Object[] messages) {
			Collection<Subscriber> subscribers = this.observers.get(topic);
			if (subscribers != null) {
				/*
				 * Make a copy to prevent {@link ConcurrentModificationException}
				 * if inside a listeners a new listener is subscribed.
				 */
				for (final Subscriber subscriber : subscribers) {
					subscriber.receive(topic, messages);
				}
			}
		}

		@Override
		public void clear() {
			this.observers.clear();
		}

	}

}
