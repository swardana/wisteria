/**
 * The wisteria module descriptor.
 *
 * @moduleGraph
 *
 * @author Sukma Wardana
 * @since 1.0
 */
module com.swardana.wisteria {

	requires javafx.base;
	requires javafx.controls;
	requires javafx.graphics;

	exports com.swardana.wisteria;

}
