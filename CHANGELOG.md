# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

- A `visual proxy` for `Object` _business attributes_.
- A centralized pub sub mediator to deliver messages on specific topics across subscribers.
- An _Object-Oriented_ way to manage menu application.
